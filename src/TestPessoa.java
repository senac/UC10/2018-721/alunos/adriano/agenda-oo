
import java.util.ArrayList;
import java.util.List;

public class TestPessoa {

    public static void main(String[] args) {

        List<Pessoa> listaPessoa = new ArrayList<>();

        Pessoa Adriano = new Pessoa("Adriano", "(27) 9.999-9999");
        Pessoa Andressa = new Pessoa("Andressa", "(27) 9.8888-8888");
        Pessoa Elza = new Pessoa("Elza", "(27) 9.7777-7777");
        Pessoa Hely = new Pessoa("Hely", "(27) 9.6666-6666");
        Pessoa Thiago = new Pessoa("Thiago", "(27) 9.999-9999");

        listaPessoa.add(Adriano);
        listaPessoa.add(Andressa);
        listaPessoa.add(Elza);
        listaPessoa.add(Hely);
        listaPessoa.add(Thiago);

        for (Pessoa contato : listaPessoa) {
            System.out.println(contato);
        }

        //Verificando se um contato já existe na lista (Método: equals(Object object)):
        if (listaPessoa.contains(Adriano)) {
            //listaPessoa.add(Adriano);
            System.out.println("Esta na lista");
        }

        Pessoa.Remover(Adriano, listaPessoa);

        for (Pessoa contato : listaPessoa) {
            System.out.println(contato);
        }

    }

}
