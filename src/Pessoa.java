
import java.util.List;
import java.util.Scanner;

public class Pessoa {

    private String nome;
    private String telefone;

    public Pessoa(String nome, String telefone) {
        this.nome = nome;
        this.telefone = telefone;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    //METODO QUE MOSTRA COMO O OBJETO SERA IMPRESSO:
    public String toString() {
        return ("Nome: " + this.nome + " - Telefone: " + this.telefone);
    }

    //METODO PARA COMPARAR PESSOAS:
    public boolean equals(Object object) {
        if (object instanceof Pessoa) {
            Pessoa pessoa = (Pessoa) object;
            return ((this.nome.equals(pessoa.getNome())) && (this.telefone.equals(pessoa.getTelefone())));
        }
        return false;
    }

    //FUNCAO PARA PROCURAR UM CONTATO E SE ELE EXISTIR E REMOVER DA LISTA:
    public static void Remover(Pessoa contato, List<Pessoa> lista) {

        Boolean nomeRemoverExiste = false;
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < lista.size(); i++) {
            if (lista.contains(contato)) {
                lista.remove(contato);
                System.out.println("O contato foi removido da agenda!");
                break;
            } else {
                nomeRemoverExiste = true;
            }
        }

        if (nomeRemoverExiste == true) {
            System.out.println("O contato nao exite na agenda");
        }
    }

}
